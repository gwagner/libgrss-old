/*
 * Copyright (C) 20092015, Roberto Guido <rguido@src.gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __FEEDS_STORE_H__
#define __FEEDS_STORE_H__

#include "libgrss.h"

#define GRSS_FEEDS_STORE_TYPE (grss_feeds_store_get_type())
G_DECLARE_DERIVABLE_TYPE (GrssFeedsStore, grss_feeds_store, GRSS, FEEDS_STORE, GObject)

struct _GrssFeedsStoreClass {
  GObjectClass parent;

  GList* (*get_channels) (GrssFeedsStore *store);
  GList* (*get_items_by_channel) (GrssFeedsStore *store, GrssFeedChannel *channel);
  gboolean (*has_item) (GrssFeedsStore *store, GrssFeedChannel *channel, const gchar *id);
  void (*add_item_in_channel) (GrssFeedsStore *store, GrssFeedChannel *channel, GrssFeedItem *item);
};

GList*    grss_feeds_store_get_channels    (GrssFeedsStore *store);
GList*    grss_feeds_store_get_items_by_channel  (GrssFeedsStore *store, GrssFeedChannel *channel);
gboolean  grss_feeds_store_has_item    (GrssFeedsStore *store, GrssFeedChannel *channel, const gchar *id);
void    grss_feeds_store_add_item_in_channel  (GrssFeedsStore *store, GrssFeedChannel *channel, GrssFeedItem *item);
void    grss_feeds_store_switch      (GrssFeedsStore *store, gboolean run);

#endif /* __FEEDS_STORE_H__ */
