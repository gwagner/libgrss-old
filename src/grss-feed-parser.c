/*
 * Copyright (C) 2009-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

/*
 * Original code is from Liferea:
 *
 * feed_parser.c  parsing of different feed formats
 * Copyright (C) 2008 Lars Lindner <lars.lindner@gmail.com>
 */

#include "grss-utils.h"
#include "grss-feed-parser.h"
#include "grss-feed-handler.h"

#include "grss-feed-rss-handler.h"
#include "grss-feed-atom-handler.h"
#include "grss-feed-pie-handler.h"

/**
 * SECTION: feed-parser
 * @short_description: feed parser
 *
 * The #GrssFeedParser is a wrapper to the many handlers available: given a
 * #GrssFeedChannel provides to identify his type and invoke the correct parser.
 */

#define FEED_PARSER_ERROR    grss_feed_parser_error_quark()

typedef struct {
  GSList *handlers;
} GrssFeedParserPrivate;

enum {
  FEED_PARSER_PARSE_ERROR,
  FEED_PARSER_FORMAT_ERROR
};

G_DEFINE_TYPE_WITH_PRIVATE (GrssFeedParser, grss_feed_parser, G_TYPE_OBJECT)

static GQuark
grss_feed_parser_error_quark ()
{
  return g_quark_from_static_string ("grss_feed_parser_error");
}

static void
grss_feed_parser_finalize (GObject *object)
{
  GrssFeedParser *parser = GRSS_FEED_PARSER (object);
  GrssFeedParserPrivate *priv = grss_feed_parser_get_instance_private (parser);

  g_slist_free_full (priv->handlers, g_object_unref);

  G_OBJECT_CLASS (grss_feed_parser_parent_class)->finalize (object);
}

static void
grss_feed_parser_class_init (GrssFeedParserClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = grss_feed_parser_finalize;
}

static void
grss_feed_parser_init (GrssFeedParser *object)
{
}

static GSList*
feed_parsers_get_list (GrssFeedParser *parser)
{
  GrssFeedHandler *feed;
  g_autoptr(GrssNSHandler) ns;
  GrssFeedParserPrivate *priv = grss_feed_parser_get_instance_private (parser);

  if (priv->handlers == NULL) {
    /*
      TODO  Parsers may be dynamically loaded and managed as external plugins
    */
    ns = grss_ns_handler_new ();

    feed = GRSS_FEED_HANDLER (grss_feed_rss_handler_new ());
    grss_feed_handler_set_ns_handler (feed, ns);
    priv->handlers = g_slist_append (priv->handlers, feed);

    feed = GRSS_FEED_HANDLER (grss_feed_atom_handler_new ());          /* Must be before pie */
    grss_feed_handler_set_ns_handler (feed, ns);
    priv->handlers = g_slist_append (priv->handlers, feed);

    feed = GRSS_FEED_HANDLER (grss_feed_pie_handler_new ());
    grss_feed_handler_set_ns_handler (feed, ns);
    priv->handlers = g_slist_append (priv->handlers, feed);
  }

  return priv->handlers;
}

/**
 * grss_feed_parser_new:
 *
 * Allocates a new #GrssFeedParser.
 *
 * Returns: a new #GrssFeedParser.
 */
GrssFeedParser*
grss_feed_parser_new ()
{
  GrssFeedParser *parser;

  parser = g_object_new (GRSS_FEED_PARSER_TYPE, NULL);
  return parser;
}

static GrssFeedHandler*
retrieve_feed_handler (GrssFeedParser *parser, xmlDocPtr doc, xmlNodePtr cur)
{
  GSList *iter;
  GrssFeedHandler *handler;

  iter = feed_parsers_get_list (parser);

  while (iter) {
    handler = (GrssFeedHandler*) (iter->data);

    if (handler && grss_feed_handler_check_format (handler, doc, cur))
      return handler;

    iter = g_slist_next (iter);
  }

  return NULL;
}

static GrssFeedHandler*
init_parsing (GrssFeedParser *parser, xmlDocPtr doc, GError **error)
{
  xmlNodePtr cur;
  GrssFeedHandler *handler;

  handler = NULL;

  do {
    if ((cur = xmlDocGetRootElement (doc)) == NULL) {
      g_set_error (error, FEED_PARSER_ERROR, FEED_PARSER_PARSE_ERROR, "Empty document!");
      break;
    }

    while (cur && xmlIsBlankNode (cur))
      cur = cur->next;

    if (!cur) {
      g_set_error (error, FEED_PARSER_ERROR, FEED_PARSER_PARSE_ERROR, "Empty XML document!");
      break;
    }

    if (!cur->name) {
      g_set_error (error, FEED_PARSER_ERROR, FEED_PARSER_PARSE_ERROR, "Invalid XML!");
      break;
    }

    handler = retrieve_feed_handler (parser, doc, cur);
    if (handler == NULL) {
      g_set_error (error, FEED_PARSER_ERROR, FEED_PARSER_FORMAT_ERROR, "Unknow format");
      break;
    }

  } while (0);

  return handler;
}

/**
 * grss_feed_parser_parse:
 * @parser: a #GrssFeedParser.
 * @feed: a #GrssFeedChannel to be parsed.
 * @doc: XML document extracted from the contents of the feed, which must
 *       already been fetched.
 * @error: location for eventual errors.
 *
 * Parses the given XML @doc, belonging to the given @feed, to obtain a list
 * of #GrssFeedItem.
 *
 * Returns: (element-type GrssFeedItem) (transfer full): a list of
 * #GrssFeedItem, to be freed when no longer in use, or NULL if anerror occours
 * and @error is set.
 */
GList*
grss_feed_parser_parse (GrssFeedParser *parser, GrssFeedChannel *feed, xmlDocPtr doc, GError **error)
{
  GrssFeedHandler *handler;

  handler = init_parsing (parser, doc, error);

  if (handler != NULL)
    return grss_feed_handler_parse (handler, feed, doc, TRUE, error);
  else
    return NULL;
}

/**
 * grss_feed_parser_parse_channel:
 * @parser: a #GrssFeedParser.
 * @feed: a #GrssFeedChannel to be parsed.
 * @doc: XML document extracted from the contents of the feed, which must
 *       already been fetched.
 * @error: location for eventual errors.
 *
 * Parses the given XML @doc, belonging to the given @feed.
 *
 * Similar to grss_feed_parser_parse(), but grss_feed_parser_parse_channel()
 * skips parsing of items into the document.
 */
void
grss_feed_parser_parse_channel (GrssFeedParser *parser, GrssFeedChannel *feed, xmlDocPtr doc, GError **error)
{
  GrssFeedHandler *handler;

  handler = init_parsing (parser, doc, error);
  if (handler != NULL)
    grss_feed_handler_parse (handler, feed, doc, FALSE, error);
}

