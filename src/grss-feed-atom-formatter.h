/*
 * Copyright (C) 2015, Roberto Guido <rguido@src.gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __GRSS_FEED_ATOM_FORMATTER_H__
#define __GRSS_FEED_ATOM_FORMATTER_H__

#include "libgrss.h"

#define GRSS_FEED_ATOM_FORMATTER_TYPE (grss_feed_atom_formatter_get_type ())
G_DECLARE_FINAL_TYPE (GrssFeedAtomFormatter, grss_feed_atom_formatter, GRSS, FEED_ATOM_FORMATTER, GrssFeedFormatter)

struct _GrssFeedAtomFormatter {
  GrssFeedFormatter parent;
};

struct _GrssFeedAtomFormatterClass {
  GrssFeedFormatterClass parent;
};

GrssFeedAtomFormatter*  grss_feed_atom_formatter_new    ();

#endif /* __GRSS_FEED_ATOM_FORMATTER_H__ */

