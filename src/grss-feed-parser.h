/*
 * Copyright (C) 2009-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __FEED_PARSER_H__
#define __FEED_PARSER_H__

#include "libgrss.h"

#define GRSS_FEED_PARSER_TYPE (grss_feed_parser_get_type ())
G_DECLARE_FINAL_TYPE (GrssFeedParser, grss_feed_parser, GRSS, FEED_PARSER, GObject)

struct _GrssFeedParser {
  GObject parent;
};

struct _GrssFeedParserClass {
  GObjectClass parent;
};

GrssFeedParser*  grss_feed_parser_new    ();

GList*    grss_feed_parser_parse    (GrssFeedParser *parser, GrssFeedChannel *feed, xmlDocPtr doc, GError **error);
void    grss_feed_parser_parse_channel  (GrssFeedParser *parser, GrssFeedChannel *feed, xmlDocPtr doc, GError **error);

#endif /* __FEED_PARSER_H__ */
