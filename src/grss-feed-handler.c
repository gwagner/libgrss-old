/*
 * Copyright (C) 2009-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#include "grss-utils.h"
#include "grss-feed-handler.h"

G_DEFINE_INTERFACE (GrssFeedHandler, grss_feed_handler, G_TYPE_OBJECT)

static void
grss_feed_handler_default_init (GrssFeedHandlerInterface *iface)
{
}

void
grss_feed_handler_set_ns_handler (GrssFeedHandler *self, GrssNSHandler *handler)
{
  if (GRSS_IS_FEED_HANDLER (self) == FALSE)
    return;

  return GRSS_FEED_HANDLER_GET_IFACE (self)->set_ns_handler (self, handler);
}

gboolean
grss_feed_handler_check_format (GrssFeedHandler *self, xmlDocPtr doc, xmlNodePtr cur)
{
  if (GRSS_IS_FEED_HANDLER (self) == FALSE)
    return FALSE;

  return GRSS_FEED_HANDLER_GET_IFACE (self)->check_format (self, doc, cur);
}

GList*
grss_feed_handler_parse (GrssFeedHandler *self, GrssFeedChannel *feed, xmlDocPtr doc, gboolean do_items, GError **error)
{
  if (GRSS_IS_FEED_HANDLER (self) == FALSE)
    return FALSE;

  return GRSS_FEED_HANDLER_GET_IFACE (self)->parse (self, feed, doc, do_items, error);
}
