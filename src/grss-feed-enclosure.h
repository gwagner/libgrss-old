/*
 * Copyright (C) 2009-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __FEED_ENCLOSURE_H__
#define __FEED_ENCLOSURE_H__

#include "libgrss.h"

#define GRSS_FEED_ENCLOSURE_TYPE (grss_feed_enclosure_get_type ())
G_DECLARE_FINAL_TYPE (GrssFeedEnclosure, grss_feed_enclosure, GRSS, FEED_ENCLOSURE, GObject)

struct _GrssFeedEnclosure {
  GObject parent;
};

struct _GrssFeedEnclosureClass {
  GObjectClass parent;
};

GrssFeedEnclosure*  grss_feed_enclosure_new    (gchar *url);

const gchar*    grss_feed_enclosure_get_url  (GrssFeedEnclosure *enclosure);
void      grss_feed_enclosure_set_format  (GrssFeedEnclosure *enclosure, gchar *type);
const gchar*    grss_feed_enclosure_get_format  (GrssFeedEnclosure *enclosure);
void      grss_feed_enclosure_set_length  (GrssFeedEnclosure *enclosure, gsize length);
gsize      grss_feed_enclosure_get_length  (GrssFeedEnclosure *enclosure);

GFile*      grss_feed_enclosure_fetch    (GrssFeedEnclosure *enclosure, GError **error);
void      grss_feed_enclosure_fetch_async    (GrssFeedEnclosure *enclosure, GAsyncReadyCallback callback, gpointer user_data);
GFile*      grss_feed_enclosure_fetch_finish  (GrssFeedEnclosure *enclosure, GAsyncResult *res, GError **error);

#endif /* __FEED_ENCLOSURE_H__ */
