/*
 * Copyright (C) 2009-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 * Copyright (C) 2015 Igor Gnatenko <ignatenko@src.gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __LIBGRSS_H__
#define __LIBGRSS_H__

#include <libsoup/soup.h>

#include <libxml/xmlerror.h>
#include <libxml/uri.h>
#include <libxml/parser.h>
#include <libxml/entities.h>
#include <libxml/HTMLparser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "grss-person.h"
#include "grss-feed-channel.h"
#include "grss-feed-enclosure.h"
#include "grss-feed-item.h"
#include "grss-feed-parser.h"
#include "grss-feed-formatter.h"
#include "grss-feed-atom-formatter.h"
#include "grss-feed-rss-formatter.h"
#include "grss-feeds-store.h"
#include "grss-feeds-pool.h"
#include "grss-feeds-subscriber.h"
#include "grss-feeds-publisher.h"
#include "grss-feeds-group.h"

#endif /* __LIBGRSS_H__ */
