/*
 * Copyright (C) 2010-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#include "grss-utils.h"
#include "grss-feeds-group-handler.h"

G_DEFINE_INTERFACE (GrssFeedsGroupHandler, grss_feeds_group_handler, G_TYPE_OBJECT)

static void
grss_feeds_group_handler_default_init (GrssFeedsGroupHandlerInterface *iface)
{
}

const gchar*
grss_feeds_group_handler_get_name (GrssFeedsGroupHandler *self)
{
  if (GRSS_IS_FEEDS_GROUP_HANDLER (self) == FALSE)
    return NULL;

  return GRSS_FEEDS_GROUP_HANDLER_GET_IFACE (self)->get_name (self);
}

gboolean
grss_feeds_group_handler_check_format (GrssFeedsGroupHandler *self, xmlDocPtr doc, xmlNodePtr cur)
{
  if (GRSS_IS_FEEDS_GROUP_HANDLER (self) == FALSE)
    return FALSE;

  return GRSS_FEEDS_GROUP_HANDLER_GET_IFACE (self)->check_format (self, doc, cur);
}

GList*
grss_feeds_group_handler_parse (GrssFeedsGroupHandler *self, xmlDocPtr doc, GError **error)
{
  if (GRSS_IS_FEEDS_GROUP_HANDLER (self) == FALSE)
    return FALSE;

  return GRSS_FEEDS_GROUP_HANDLER_GET_IFACE (self)->parse (self, doc, error);
}

gchar*
grss_feeds_group_handler_dump (GrssFeedsGroupHandler *self, GList *channels, GError **error)
{
  if (GRSS_IS_FEEDS_GROUP_HANDLER (self) == FALSE)
    return FALSE;

  return GRSS_FEEDS_GROUP_HANDLER_GET_IFACE (self)->dump (self, channels, error);
}
